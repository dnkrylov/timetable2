package com.timetable.config;

import com.typesafe.config.ConfigFactory;

public class SourceConfig {
    private static final com.typesafe.config.Config defaultConfig = ConfigFactory.parseResources("config.properties");
    private static final SourceConfig INSTANCE = new SourceConfig();

    private SourceConfig() {}

    public String getUrl() {
        return defaultConfig.getString("broker.url");
    }

    public String getUsername() {
        return defaultConfig.getString("broker.login");
    }

    public String getPassword() {
        return defaultConfig.getString("broker.password");
    }

    public String getStationsSource() {
        return defaultConfig.getString("server.stations.url");
    }

    public String getTimeTableSource() {
        return defaultConfig.getString("server.timeTable.url");
    }

    public static SourceConfig getInstance() {
        return INSTANCE;
    }

}
