package com.timetable.jms;

import com.timetable.config.SourceConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.jms.*;

@Startup
@Singleton
@Slf4j
public class MyTopicSubscriber {
    private ActiveMQConnectionFactory connectionFactory;
    private TopicConnection subscriberConnection;
    private TopicSession subscriberSession;
    private Topic subscriberTopic;
    private TopicSubscriber subscriber;

    @Inject
    private JmsMessageListener jmsMessageListener;

    @PostConstruct
    private void init() {
        try {
            SourceConfig config = SourceConfig.getInstance();
            connectionFactory = new ActiveMQConnectionFactory(config.getUrl());
            connectionFactory.setUserName(config.getUsername());
            connectionFactory.setPassword(config.getPassword());
            subscriberConnection = connectionFactory.createTopicConnection();
            subscriberSession = subscriberConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
            subscriberTopic = subscriberSession.createTopic("TIMETABLE_CHANGED");
            subscriber = subscriberSession.createSubscriber(subscriberTopic);
            subscriber.setMessageListener(jmsMessageListener);
            subscriberConnection.start();
        } catch (JMSException e) {
            log.error(e.getMessage(), e);
        }
        log.info("TOPIC SUBSCRIBER INITIALIZED");
    }

    @PreDestroy
    private void close(){
        try {
            subscriber.close();
            subscriberSession.close();
            subscriberConnection.close();
        } catch (JMSException e) {
            log.error(e.getMessage(), e);
        }
        log.info("TOPIC SUBSCRIBER DESTROYED");
    }
}
