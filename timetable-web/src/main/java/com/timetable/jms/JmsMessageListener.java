package com.timetable.jms;

import com.timetable.jspbeans.TimeTableBean;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
@Slf4j
public class JmsMessageListener implements MessageListener, Serializable {

    private static final List<TimeTableBean> timeTables = new ArrayList<>();

    @Override
    public void onMessage(Message message) {
        TextMessage textMessage = (TextMessage) message;
        try {
            String text = textMessage.getText();
            log.info("MESSAGE '" + text + "' RECEIVED");
            timeTables.stream().forEach(timeTableBean -> timeTableBean.messageReceived(text));
        } catch (JMSException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void subscribe(TimeTableBean timeTableBean) {
        timeTables.add(timeTableBean);
        log.info("TIMETABLE BEAN " + timeTableBean +
                "SUBSCRIBED TO MESSAGE LISTENER, SUBSCRIBERS AMOUNT: " + timeTables.size());
    }

    public void unSubscribe(TimeTableBean timeTableBean) {
        timeTables.remove(timeTableBean);
        log.info("TIMETABLE BEAN " + timeTableBean +
                "UNSUBSCRIBED FROM MESSAGE LISTENER, SUBSCRIBERS AMOUNT: " + timeTables.size());
    }
}
