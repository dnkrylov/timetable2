package com.timetable.jspbeans;

import com.timetable.config.SourceConfig;
import com.timetable.jms.JmsMessageListener;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.SessionScoped;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;


@SessionScoped
@Named
@Data
@Slf4j
public class TimeTableBean implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(TimeTableBean.class);
    private String getStationsPath;
    private String getTimeTablePath;

    @Inject
    private JmsMessageListener jmsMessageListener;

    @Inject
    @Push
    PushContext updateChannel;

    @PostConstruct
    private void subscribe() {
        SourceConfig config = SourceConfig.getInstance();
        jmsMessageListener.subscribe(this);
        getStationsPath = config.getStationsSource();
        getTimeTablePath = config.getTimeTableSource();
    }

    @PreDestroy
    private void unSubscribe() {
        jmsMessageListener.unSubscribe(this);
    }

    public void messageReceived(String message) {
        updateChannel.send(message);
        log.info("MESSAGE " + message + " RECEIVED AND SENT TO FRONTEND");
    }

}
