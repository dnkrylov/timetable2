let stations = {
    data: [],
    list: {
        match: {
            enabled: true
        }
    }
};
let selectedStation;

$(document).ready(function () {
    getStations();
    $("#selectStationForTimeTable").change(function () {
        selectedStation = $("#selectStationForTimeTable").val();
        $('#timetableContainer').empty();
        $('#alertErrorStation').empty();
        if (!stations.data.includes(selectedStation) || selectedStation === '') {
            $('#alertErrorStation').append('SUCH STATION DOES NOT EXISTS');
            return;
        } else {
            updateTimeTable()
        }
    });
});

function updateStations(message) {
    if (!stations.data.includes(message)) {
        stations.data.push(message);
    }
}

function getStations() {
    $.ajax({
        type: 'GET',
        url: $('#getStationsPath').val(),
        success: function (data) {
            for (let i = 0; i < data.length; i++) {
                stations.data.push(data[i]);
            }
            $('#selectStationForTimeTable').easyAutocomplete(stations);
        },
        error: function () {
            $('#alertErrorStation').append('CANT CONNECT TO SERVER, PLEASE TRY AGAIN LATER');
        }
    })
}

function updateTimeTable() {

    $.ajax({
        type: 'GET',
        url: $('#getTimeTablePath').val(),
        data: {
            stationName: selectedStation,
            date: updateDate()
        },
        success: function (data) {
            showTimeTable(data);
        },
        error: function () {
            $('#alertErrorStation').append('CANT CONNECT TO SERVER, PLEASE TRY AGAIN LATER');
        }
    })
}

function showTimeTable(data) {
    $('#alertErrorStation').empty();
    $('#timeTableContainer').empty();
    if (data.length === 0) {
        $('#alertErrorStation').append("NO TRAINS FOUND DEPARTING OR ARRIVING FROM THIS STATION THIS DATE");
        return;
    }
    let table = document.createElement('table');
    table.className = "table table-hover text-center";
    table.id = "timeTable";
    $('#timeTableContainer').append(table);

    let header = document.createElement('thead');
    header.className = "text-center";
    header.id = "timeTable";
    header.innerHTML = '<thead class="text-center">' +
        '<th>TRAIN NUMBER</th><th>ARRIVAL TIME</th><th>DEPARTURE TIME</th>' +
        '</thead>';
    table.append(header);

    let body = document.createElement('tbody');
    body.id = "timeTableBody";
    table.append(body);

    for (let i = 0; i < data.length; i++) {
        let tr = document.createElement('tr');
        tr.innerHTML = `<td>${data[i].trainNumber}</td><td>${data[i].arrivalTime}</td><td>${data[i].departureTime}</td>`;
        body.append(tr);
    }
}

function updateDate() {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2,'0');
    let mm = String(today.getMonth() + 1).padStart(2, '0');
    let yyyy = today.getFullYear();
    return dd + '.' + mm + '.' + yyyy;
}